**ECE 9039 group 15 project, Parkinson Disease Classification**

---

There are two python files. 

1. The first one, RFE.py, is the file for RFE recursive feature elemination feature selection method used.

2. The second file, individual_feature_group.py, is the second one for the six individual feature groups are used.

---

Dataset Links

https://archive.ics.uci.edu/ml/datasets/Parkinson%27s+Disease+Classification



